import React, { Component } from 'react';
import Titles from "./Components/Titles";
import Forms from "./Components/Forms";
import Weather from "./Components/Weather";

const API_KEY = "7a5b66a6db4c8f6c108d3ba06f2dc818";//apikey

class App extends Component {
  state = {
    temperature : undefined,
    city : undefined,
    country : undefined,
    humidity : undefined,
    description : undefined,
    error : undefined
  }

  getWeather = async (e) => {//arrow function allow you to use the 'this' keyword independently which s bound to the App component
    e.preventDefault();//prevents the default behaviour of the component to refresh when button is pressed. 'e' is event object in JS
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    const api_call = await fetch (`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);//fetch is used to perform the apicall
    //``- template strings allow you to use variables you have used anywhere in your file
    const data = await api_call.json();//to convert our data to json
    if (city && country){
      //console.log(data);
      this.setState({//defining the state
        temperature : data.main.temp,
        city : data.name,
        country : data.sys.country,
        humidity : data.main.humidity,
        description : data.weather[0].description,//the weather object is different because it contains an array
        error : ""
      });

    } else {
      this.setState({
      temperature : undefined,
      city : undefined,
      country : undefined,
      humidity : undefined,
      description : undefined,//the weather object is different because it contains an array
      error : "Please enter the values"
    });

    }


  };
  render() {
    return (
      <div>
        <div className="wrapper">
          <div className="main">
            <div className="container">
              <div className="row">
                <div className="col-xs-5 title-container">
                  <Titles/>
                </div>
                <div className="col-xs-7 form-container">
                  <Forms getWeather = {this.getWeather}/>{/* to allow the Forms component to access the getWeather function by defining the prop 'getWeather'. Props act like HTML attributes*/}
                  <Weather
                    temperature = {this.state.temperature}
                    city = {this.state.city}
                    country = {this.state.country}
                    humidity = {this.state.humidity}
                    description = {this.state.description}
                    error = {this.state.error}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
};


export default App;
