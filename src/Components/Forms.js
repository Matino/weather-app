import React from "react";

const Forms = (props, search) => (
  <div>

    <form onSubmit = {props.getWeather}>
      <input
        type="text" name = "city"
        placeholder= "City..."
        ref = {input => search = input}

      />
      <input type="text" name = "country" placeholder= "Country..."/>
      <button>Get weather</button>
    </form>

  </div>
);

export default Forms;
